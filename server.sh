# Install docker ubuntu 16.04
echo 'Installing docker...'
apt-get update
apt-get dist-upgrade
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-cache policy docker-ce
apt-get install -y docker-ce

#Adding current user to the docker group
echo 'Adding current user to the docker group...'
sudo usermod -aG docker ${USER}
su - ${USER}
id -nG

# Install docker-compose
echo 'Installing docker compose...'
curl -o /usr/local/bin/docker-compose -L "https://github.com/docker/compose/releases/download/1.16.1/docker-compose-$(uname -s)-$(uname -m)"
chmod +x /usr/local/bin/docker-compose
docker-compose -v

# Install gitlab runner
echo 'Installing gitlab-runner...'
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.deb.sh -o /tmp/gl-runner.deb.sh
bash /tmp/gl-runner.deb.sh
apt-get install gitlab-runner